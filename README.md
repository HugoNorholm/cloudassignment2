# Cloud assignment 2

Deployed to Heroku https://hugonorholmassingment2.herokuapp.com/

# Implemented:
- GET paragliding/api
    api information
- POST paragliding/api/track
    Adds a new track by url
    request body:
    ```
    {
        "url": "<url>"
    }
    ```
- GET paragliding/api/track
    returns an array of all track ids
- GET paragliding/api/track/<id>
    returns all fields on a track with the given id
- GET paragliding/api/track/<id>/<field>
    return a field in a track
- GET /admin/api/tracks_count
    returns the amount of tracks stored
- DELETE /admin/api/tracks
    removes all stored trackes
- GET paragliding/api/ticker
    ticker information
- GET paragliding/api/ticker/latest
    latest ticker information
- GET paragliding/api/ticker/<timestamp>
    ticker information of a given timestamp
- POST paragliding/webhook/new_track
	Adds a webhook to the database

# Not impemented:
- Anything with posting to webhooks
- Clock trigger on OpenStack