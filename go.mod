module paragliding

require (
	github.com/globalsign/mgo v0.0.0-20181015135952-eeefdecb41b8
	github.com/gorilla/mux v1.6.2
	github.com/marni/goigc v0.1.0
	github.com/rickb777/date v1.7.2
)
