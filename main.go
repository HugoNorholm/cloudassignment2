package main

import (
	"github.com/globalsign/mgo/bson"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"
	"strconv"

	"github.com/globalsign/mgo"
	"github.com/gorilla/mux"
	"github.com/marni/goigc"
	"github.com/rickb777/date/period"
)

// APIMeta Json struct for API
type APIMeta struct {
	Uptime  period.Period `json:"uptime,omitempty"`
	Info    string        `json:"info,omitempty"`
	Version string        `json:"version,omitempty"`
}

// Track Json struct for Tracks
type Track struct {
	ID          string  `json:"ID,omitempty" bson:"_id"` 
	Pilot       string  `json:"pilot,omitempty"` 
	Glider      string  `json:"glider,omitempty"`
	GliderID    string  `json:"gliderID,omitempty"`
	TrackLenght float64 `json:"tracklength"`
	Hdate       string  `json:"H_date,omitempty"` 
	TrackSrcURL string  `json:"track_src_URL,omitempty"`
	Timestamp	int64	`json:"timestamp,omitempty"`
}

// TrackReply Json struct for trackreplies
type TrackReply struct {
	Pilot       string  `json:"pilot,omitempty"` 
	Glider      string  `json:"glider,omitempty"`
	GliderID    string  `json:"gliderID,omitempty"`
	TrackLenght float64 `json:"tracklength"`
	Hdate       string  `json:"H_date,omitempty"` 
	TrackSrcURL string  `json:"track_src_URL,omitempty"`
}

// URLBody Json struct for URLs
type URLBody struct {
	URL string `json:"URL,omitempty"`
}

// IDResponse Json struct for ID
type IDResponse struct {
	ID string `json:"ID,omitempty" bson:"_id"` 
}

// Ticker Json struct for ID
type Ticker struct {	
	Tlatest int64 `json:"t_latest,omitempty"` 
	Tstart 	int64 `json:"t_start,omitempty"` 
	Tstop 	int64 `json:"t_stop,omitempty"` 
	Tracks	[]IDResponse `json:"Tracks,omitempty"` 
	Processing	time.Duration `json:"processing_time,omitempty"` 
}

// Webhook Json struct for ID
type Webhook struct {
	WebhookURL string `json:"webhookURL,omitempty"` 
	MinTriggerValue int64 `json:"minTriggerValue,omitempty"` 
}

var session *mgo.Session
var startTime time.Time
var db *mgo.Database

//GetAPI Send API info as JSON
func GetAPI(w http.ResponseWriter, r *http.Request) {
	uptime := period.Between(time.Now(), startTime)
	x := APIMeta{Uptime: uptime, Info: "Service for Paragliding Tracks", Version: "V1"}
	json.NewEncoder(w).Encode(x)
}

//Posttrack Takes URL in json and gets track file parses it and stores it in db
func Posttrack(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var u URLBody
	err := decoder.Decode(&u)
	if err != nil {
		panic(err)
	}

	s := u.URL
	treck, err := igc.ParseLocation(s)

	temp := Track{ID: treck.UniqueID, Pilot: treck.Pilot, Glider: treck.GliderType, GliderID: treck.GliderID, TrackLenght: treck.Task.Distance(), Hdate: treck.Header.Date.String(), TrackSrcURL: u.URL, Timestamp: time.Now().UnixNano()}
	c := session.DB("hugopragliding").C("tracks")
	err = c.Insert(temp)
	if mgo.IsDup(err) {
		fmt.Println("snike dupe")
	}

	i := IDResponse{ID: treck.UniqueID}
	json.NewEncoder(w).Encode(i)
}

//GetTrack Sends all Tracks
func GetTrack(w http.ResponseWriter, r *http.Request) {
	c := session.DB("hugopragliding").C("tracks")
	var item []IDResponse

	err := c.Find(nil).Select(bson.M{"_id":1}).All(&item)

	if err != nil {
		http.Error(w, err.Error(),400)
		return
	}

	json.NewEncoder(w).Encode(item)
}

//GetTrackID Sends all info of a track with a given ID
func GetTrackID(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	c := session.DB("hugopragliding").C("tracks")
	var track TrackReply

	err := c.Find(bson.M{"_id": params["ID"]}).One(&track)

	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	json.NewEncoder(w).Encode(track)
}

//GetTrackIDField Sends specific info of a track with a given ID
func GetTrackIDField(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	c := session.DB("hugopragliding").C("tracks")
	var track Track

	err := c.Find(bson.M{"_id": params["ID"]}).One(&track)

	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}

	switch params["field"] {
	case "pilot":
		json.NewEncoder(w).Encode(track.Pilot)
		return
	case "glider":
		json.NewEncoder(w).Encode(track.Glider)
		return
	case "glider_id":
		json.NewEncoder(w).Encode(track.GliderID)
		return
	case "calculated_total_track_length":
		json.NewEncoder(w).Encode(track.TrackLenght)
		return
	case "H_date":
		json.NewEncoder(w).Encode(track.Hdate)
		return
	case "track_src_url":
		json.NewEncoder(w).Encode(track.TrackSrcURL)
		return
	}
	w.WriteHeader(http.StatusNotFound)
	w.Write([]byte("404 - NOT FOUND"))	
}

//GetAPITicker aa
func GetAPITicker(w http.ResponseWriter, r *http.Request){
	start := time.Now()

	var t Ticker

	c := session.DB("hugopragliding").C("tracks")
	var track Track

	err := c.Find(nil).Sort("-timestamp").One(&track)
	if err != nil {
		panic(err)
	}

	t.Tlatest = track.Timestamp

	err = c.Find(nil).Sort("timestamp").One(&track)
	if err != nil {
		panic(err)
	}

	t.Tstart = track.Timestamp

	err = c.Find(nil).Select(bson.M{"_id":1}).Sort("-timestamp").All(&t.Tracks)

	if err != nil {
		panic(err)
	}

	end := time.Now()
	t.Processing = end.Sub(start)
	json.NewEncoder(w).Encode(t)
}

//GetAPITickerLatest Gets the most recent timeestamp from db
func GetAPITickerLatest(w http.ResponseWriter, r *http.Request){
	c := session.DB("hugopragliding").C("tracks")
	var track Track

	err := c.Find(nil).Sort("-timestamp").One(&track)
	if err != nil {
		panic(err)
	}

	json.NewEncoder(w).Encode(track.Timestamp)
}

//GetAPITickerTimestamp get tickerinfo
func GetAPITickerTimestamp(w http.ResponseWriter, r *http.Request){
	start := time.Now()
	params := mux.Vars(r)
	var y int64
	y, err := strconv.ParseInt(params["timestamp"],10,64)
	if err != nil {
		panic(err)
	}
	var t Ticker

	c := session.DB("hugopragliding").C("tracks")
	var track Track

	err = c.Find(nil).Sort("-timestamp").One(&track)
	if err != nil {
		panic(err)
	}

	t.Tlatest = track.Timestamp

	err = c.Find(bson.M{"timestamp": bson.M{"$gte" : y }}).Sort("timestamp").One(&track)
	if err != nil {
		panic(err)
	}

	t.Tstart = track.Timestamp

	err = c.Find(bson.M{"timestamp": bson.M{"$gte" : y }}).Sort("-timestamp").All(&t.Tracks)

	if err != nil {
		panic(err)
	}

	end := time.Now()
	t.Processing = end.Sub(start)
	json.NewEncoder(w).Encode(t)
}

//PostHook register a webhook
func PostHook(w http.ResponseWriter, r *http.Request){
	decoder := json.NewDecoder(r.Body)
	var s Webhook

	err := decoder.Decode(&s)
	if err != nil {
		panic(err)
	}

	fmt.Println(s.WebhookURL)
	fmt.Println(s.MinTriggerValue)

	c := session.DB("hugopragliding").C("webhooks")
	err = c.Insert(s)
	if mgo.IsDup(err) {
		fmt.Println("snike dupe")
	}

	json.NewEncoder(w).Encode(s)
}

//GetHook aa
func GetHook(w http.ResponseWriter, r *http.Request){}

//DeleteHook aa
func DeleteHook(w http.ResponseWriter, r *http.Request){
	c := session.DB("hugopragliding").C("Webhooks")

	err := c.Remove(bson.M{"mintriggervalue": "1"})
	if err != nil {
		fmt.Printf("remove failed")
	}
}

//GetAdminTrackCount gets amount of Tracks in the database
func GetAdminTrackCount(w http.ResponseWriter, r *http.Request){
	c := session.DB("hugopragliding").C("Tracks")
	i,err := c.Count()
	if err != nil {
		panic(err)
	}
	fmt.Println(i)
}

//DeleteTracks deletes all Tracks in the database
func DeleteTracks(w http.ResponseWriter, r *http.Request){
	c := session.DB("hugopragliding").C("Tracks")
	err := c.DropCollection() 
	if err != nil {
		panic(err)
	}
}

func determineListenAddress() (string, error) {
	port := os.Getenv("PORT")
	if port == "" {
		return "", fmt.Errorf("$PORT not set")
	}
	return ":" + port, nil
}

func main() {
	startTime = time.Now()
	addr, err := determineListenAddress()
	if err != nil {
		log.Fatal(err)
	}

	session, err = mgo.Dial("mongodb://admin:adminpass1@ds255970.mlab.com:55970/hugopragliding")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	r := mux.NewRouter()

	r.HandleFunc("/paragliding/API", GetAPI).Methods("GET")
	r.HandleFunc("/paragliding/API/track", Posttrack).Methods("POST")
	r.HandleFunc("/paragliding/API/track", GetTrack).Methods("GET")
	r.HandleFunc("/paragliding/API/track/{ID}", GetTrackID).Methods("GET")
	r.HandleFunc("/paragliding/API/track/{ID}/{field}", GetTrackIDField).Methods("GET")

	r.HandleFunc("/paragliding/API/ticker", GetAPITicker).Methods("GET")
	r.HandleFunc("/paragliding/API/ticker/latest", GetAPITickerLatest).Methods("GET")
	r.HandleFunc("/paragliding/API/ticker/{timestamp}", GetAPITickerTimestamp).Methods("GET")

	r.HandleFunc("/paragliding/webhook/new_track", PostHook).Methods("POST")
	r.HandleFunc("/paragliding/webhook/new_track/{webhook_id}", GetHook).Methods("GET")
	r.HandleFunc("/paragliding/webhook/new_track/{webhook_id}", DeleteHook).Methods("DELETE")

	r.HandleFunc("/paragliding/admin/API/tracks_count", GetAdminTrackCount).Methods("GET")
	r.HandleFunc("/paragliding/admin/API/Tracks", DeleteTracks).Methods("DELETE")

    ticker := time.NewTicker(5 * time.Minute)
    go func() {
        for t := range ticker.C {

			c := session.DB("hugopragliding").C("Tracks")
			var item []IDResponse
		
			err := c.Find(nil).Select(bson.M{"_id":1}).All(&item)
			b, err := json.Marshal(item)
			v := url.Values{}
			v.Set("content", string(b))
		
			hook := "https://discordapp.com/api/webhooks/505300435720994835/OL-uhrjsDihpGlvcb3OyNhFKk_dBrANZjp2w5DNlZh_CkDeR_E2Zfi5KpfC2f6IpkLvf"
			resp, err := http.PostForm(hook, v)
			fmt.Println(resp)
			fmt.Println(t)
			fmt.Println(err)
        }
    }()

	log.Printf("Listening on %s...\n", addr)
	if err := http.ListenAndServe(addr, r); err != nil {
		panic(err)
	}
}
